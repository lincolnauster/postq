# Note that this flake is *not* for packaging postq, given that Cargo (sigh)
# takes care of that. Rather, this flake wraps up some generic DevOps workflows
# and development pipelines.
#
# This Source Code Form is subject to the terms of the Mozilla Public License,
# v. 2.0. If a copy of the MPL was not distributed with this file, You can
# obtain one at https://mozilla.org/MPL/2.0/.

{
  inputs.nixpkgs.url = "github:nixos/nixpkgs";

  inputs.fenix.url = "github:nix-community/fenix";
  inputs.fenix.inputs.nixpkgs.follows = "nixpkgs";

  outputs = { self, nixpkgs, fenix }: let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
    };

    rust_tc = fenix.packages.x86_64-linux.fromToolchainFile {
      file = ./rust-toolchain.toml;
      sha256 = "sha256-pTeY2g4aQ4X2n1XUSE035EHhuDdQyDS/FM+oyHRP/f8=";
    };

    rust = pkgs.makeRustPlatform {
      rustc = rust_tc;
      cargo = rust_tc;
    };

    grcov = rust.buildRustPackage {
      name = "grcov";
      ver = "0.8.2";
      src = pkgs.fetchFromGitHub {
        owner  = "mozilla";
        repo = "grcov";
        rev = "v0.8.2";
        sha256 = "sha256-t1Gj5u4MmXPbQ5jmO9Sstn7aXJ6Ge+AnsmmG2GiAGKE=";
      };

      cargoSha256 = "sha256-AExv8nXcYir97MbFh8Vj9rWM9gXiINqDgLVyAmCY4G0=";

      doCheck = false;
    };

    rust_stable = fenix.packages.x86_64-linux.stable;
  in {
    packages.x86_64-linux.codeCoverage = rust.buildRustPackage {
      name = "postq-code-coverage";
      ver = "0.0.1";
      src = ./.;
      cargoSha256 = "sha256-tjeIfXIr0GhGhoAdgwk9pbfVe5lmVlb/LVeL0quAGE4=";

      cargoPatches = [ ./Cargo.lock.patch ];

      installPhase = ''
        RUSTFLAGS="-Z instrument-coverage" cargo test
        ${grcov}/bin/grcov . --binary-path ./target/debug/ \
                             -s . --branch --ignore-not-existing \
                             -o ./coverage -t html
        mkdir -p $out/
        cp -r ./default.profraw $out/
        cp -r coverage $out/
      '';

      nativeBuildInputs = with pkgs; [ pkgs.tree pkgs.llvmPackages_13.bintools ];
    };

    devShell.x86_64-linux = pkgs.mkShell {
      buildInputs = [
        rust_stable.cargo rust_stable.rustc rust_stable.clippy
      ];
    };

    devShells.x86_64-linux.nightly = pkgs.mkShell {
      buildInputs = [ rust_tc ];
    };
  };
}
