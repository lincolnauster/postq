// This Source Code Form is subject to the terms of the Mozilla Public License,
// v. 2.0. If a copy of the MPL was not distributed with this file, You can
// obtain one at https://mozilla.org/MPL/2.0/.

//! This crate provides a persistent queue with good performance for both
//! in-memory and on-disk reads and writes.

#![deny(missing_docs)]

use std::io::Result;
use std::path::PathBuf;
use std::mem;
use std::sync::{Arc, Mutex};

mod queue_file;
mod threading;

/// A function pointer to a deserializer for any data.
// XXX: should this be a Result<T, (generic parse error type)>?
pub type Deserializer<T> = fn(&[u8]) -> Option<T>;
/// A function pointer to a serializer for any data.
pub type Serializer<T> = fn(&T) -> Vec<u8>;

/// This is what you'll always want to use to create a persistent queue. It
/// balances in-memory and on-filesystem information, and manages concurrency,
/// the file, and a cache.
///
/// It stores a CACHE_SIZE number of prefetched items on the stack (well, as
/// much on the stack as is possible. If your deserializer returns a Box,
/// there's not much the Queue can do).
pub struct Queue<T, const CACHE_SIZE: usize> {
    file: Arc<Mutex<queue_file::QueueFile>>,
    buf: [Option<T>; CACHE_SIZE],
    deserializer: Deserializer<T>,
    serializer: Serializer<T>,
    sync_pool: threading::TaskPool<Result<()>>
}

impl<T, const CACHE_SIZE: usize> Queue<T, CACHE_SIZE> {
    /// Given a path to open and a way to deserialize data, construct a Queue.
    /// This is O(n) with respect to CACHE_SIZE.
    pub fn new(
        path: PathBuf, deserializer: Deserializer<T>, serializer: Serializer<T>
    ) -> Result<Self> {
        let mut file = queue_file::QueueFile::new(path)?;

        let mut buf: [Option<T>; CACHE_SIZE] = unsafe {
            mem::MaybeUninit::uninit().assume_init()
        };

        for (i, item) in buf.iter_mut().enumerate() {
            *item = file.get(i)?.map(|x| deserializer(&x)).flatten();
        }

        Ok(Self {
            file: Arc::new(Mutex::new(file)),
            buf: buf,
            deserializer: deserializer,
            serializer: serializer,
            sync_pool: threading::TaskPool::empty(),
        })
    }

    /// Your standard dequeue operation: remove and return the item at the front
    /// of the queue. This will return immediately, but will spawn a thread to
    /// update the file. If you need to make sure that the file is updated, you
    /// can either `std::mem::drop` your queue to force a sync, or call `sync`
    /// yourself, which returns any errors it comes across.
    pub fn dequeue(&mut self) -> Result<Option<T>> {
        let mut ret_buf = None;

        if self.buf[0].is_some() {
            mem::swap(&mut ret_buf, &mut self.buf[0]);
            self.sync();
            let next = self.file.lock().unwrap().get(CACHE_SIZE)?;
            self.buf[0] = next.map(|x| (self.deserializer)(&x)).flatten();
            self.buf.rotate_left(1);

            let file = self.file.clone();
            self.sync_pool.run(move || {
                file.lock().unwrap().dequeue().map(|_| ())
            });
        }

        Ok(ret_buf)
    }

    /// Your standard enqueue operation: add the target to the cache and, in a
    /// concurrent thread, the file. See dequeue docs for information on manual
    /// syncing.
    pub fn enqueue(&mut self, target: T) {
        let bytes = (self.serializer)(&target);

        self.buf.iter_mut().find(|x| x.is_none()).map(|x| *x = Some(target));

        let file = self.file.clone();
        self.sync_pool.run(move || {
            file.lock().unwrap().enqueue(&bytes)
        });
    }

    /// Because file writes are done in parallel (or at worst concurrently) with
    /// the main thread, there's no general run-time guarantee about the state of
    /// the file. This method blocks until all filesystem-backed operations are
    /// completed. If they return any errors, they'll be present in the return
    /// value.
    pub fn sync(&mut self) -> Vec<Result<()>> {
        self.sync_pool.join()
    }
}

impl<T, const CACHE_SIZE: usize> Drop for Queue<T, CACHE_SIZE> {
    /// We'll automatically finish any fs operations when the queue goes out of
    /// scope. Note, however, that this *completely* ignores any errors.
    fn drop(&mut self) {
        self.sync();
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::Write;
    use std::fs::{File, remove_file};

    #[test]
    /// Make sure that we can read a simple queue storing 2 values.
    fn basic_init_and_dequeue() {
        const PATH: &str = "/tmp/postq-testing-basic_init.bin";
        let data = [
            0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 2 values
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // no next
            0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // first value index
            0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 4 bytes
            0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // second value index
            0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 4 bytes
            0x01, 0x00, 0x00, 0x00, // store 1 in a 32-bit int
            0x02, 0x00, 0x00, 0x00, // store 2 in a 32-bit int
        ];

        File::create(PATH)
            .unwrap()
            .write_all(&data)
            .ok();

        let mut q = Queue::<i32, 2>::new(PathBuf::from(PATH),
            |x: &[u8]| Some(i32::from_le_bytes(x.try_into().expect("slice with correct length"))),
            |x: &i32| x.to_le_bytes().to_vec(),
        ).unwrap();

        assert_eq!(q.dequeue().unwrap(), Some(1));
        assert_eq!(q.dequeue().unwrap(), Some(2));
        assert_eq!(q.dequeue().unwrap(), None);

        remove_file(PATH).unwrap();
    }

    #[test]
    /// Make sure that we can read a simple queue storing 2 values.
    fn basic_init_and_enqueue() {
        const PATH: &str = "/tmp/postq-testing-basic_enqueue.bin";
        let data = [
            0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 2 values
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // no next
            0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // first value index
            0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 4 bytes
            0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // second value index
            0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 4 bytes
            0x01, 0x00, 0x00, 0x00, // store 1 in a 32-bit int
            0x02, 0x00, 0x00, 0x00, // store 2 in a 32-bit int
        ];

        File::create(PATH)
            .unwrap()
            .write_all(&data)
            .ok();

        let mut q = Queue::<i32, 2>::new(PathBuf::from(PATH),
            |x: &[u8]| Some(i32::from_le_bytes(x.try_into().expect("slice with correct length"))),
            |x: &i32| x.to_le_bytes().to_vec(),
        ).unwrap();

        q.enqueue(3);
        q.enqueue(4);
        assert_eq!(q.dequeue().unwrap(), Some(1));
        assert_eq!(q.dequeue().unwrap(), Some(2));
        assert_eq!(q.dequeue().unwrap(), Some(3));
        assert_eq!(q.dequeue().unwrap(), Some(4));
        assert_eq!(q.dequeue().unwrap(), None);

        remove_file(PATH).unwrap();
    }

    #[test]
    fn more_cache_than_elements() {
        const PATH: &str = "/tmp/postq-testing-more_cache_than_elements.bin";
        let data = [
            0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 2 values
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // no next
            0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // first value index
            0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 4 bytes
            0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // second value index
            0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 4 bytes
            0x01, 0x00, 0x00, 0x00, // store 1 in a 32-bit int
            0x02, 0x00, 0x00, 0x00, // store 2 in a 32-bit int
        ];

        File::create(PATH)
            .unwrap()
            .write_all(&data)
            .ok();

        let mut q = Queue::<i32, 2>::new(PathBuf::from(PATH),
            |x: &[u8]| Some(i32::from_le_bytes(x.try_into().expect("slice with correct length"))),
            |x: &i32| x.to_le_bytes().to_vec(),
        ).unwrap();

        assert_eq!(q.dequeue().unwrap(), Some(1));
        assert_eq!(q.dequeue().unwrap(), Some(2));
        assert_eq!(q.dequeue().unwrap(), None);

        remove_file(PATH).unwrap();
    }

    #[test]
    /// make sure we can repopulate the cache after exhaustion.
    fn less_cache_than_elements() {
        const PATH: &str = "/tmp/postq-testing-less_cache_than_elements.bin";

        let data = [
            0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

            0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x64, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x68, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x6c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x70, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

            0x01, 0x00, 0x00, 0x00,
            0x02, 0x00, 0x00, 0x00,
            0x03, 0x00, 0x00, 0x00,
            0x04, 0x00, 0x00, 0x00,
            0x05, 0x00, 0x00, 0x00,
        ];

        File::create(PATH)
            .unwrap()
            .write_all(&data)
            .ok();

        let mut q = Queue::<i32, 2>::new(PathBuf::from(PATH),
            |x: &[u8]| Some(i32::from_le_bytes(x.try_into().expect("slice with correct length"))),
            |x: &i32| x.to_le_bytes().to_vec(),
        ).unwrap();

        assert_eq!(q.dequeue().unwrap(), Some(1));
        assert_eq!(q.dequeue().unwrap(), Some(2));
        assert_eq!(q.dequeue().unwrap(), Some(3));
        assert_eq!(q.dequeue().unwrap(), Some(4));
        assert_eq!(q.dequeue().unwrap(), Some(5));

        remove_file(PATH).unwrap();
    }
}
