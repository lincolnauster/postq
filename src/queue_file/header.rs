use std::fs::{File};
use std::io::{Read, Write, Seek, SeekFrom};
use std::num::NonZeroU64;
use std::mem::size_of;

use smallvec::SmallVec;

use super::Result;

const QUEUE_HEADER_INIT_SIZE: usize = 32;

/// Store metadata about bytes in a location. This is all required information
/// about where an element is and how much space it takes up.
#[derive(Copy, Clone)]
pub struct ItemLocation {
    pub offset: u64,
    pub length: u64,
}

/// Store the location of an item.
#[derive(Copy, Clone)]
enum ItemIndex {
    BlockDelim,
    Item(ItemLocation),
}

/// Store metadata about a QueueFile header. This roughly encapsulates
/// everything about the header saving the precise indices and file IO.
pub struct Header(SmallVec<[ItemIndex; QUEUE_HEADER_INIT_SIZE]>);

impl Header {
    /// Construct an instance of Self by parsing an existing file containing the
    /// data necessary for a full queue.
    pub fn parse_header(file: &mut File) -> Result<Self> {
        let mut header = SmallVec::new();
        let mut next_block = 0;

        loop {
            let (addrs, following) = Self::parse_header_block(file, next_block)?;
            header.extend(addrs);

            if let Some(following) = following {
                next_block = following.get();
                header.push(ItemIndex::BlockDelim);
            } else {
                break;
            }
        }

        Ok(Self(header))
    }

    /// Write the whole of the header to a given file. Note that this mostly
    /// defers to the private associated function [Self::write_block].
    pub fn write(&self, file: &mut File) -> Result<()> {
        let mut file_index = 0;
        let mut self_index = 0;
        while let Some((a, b)) = self.write_block(file, self_index, file_index)? {
            file_index = a;
            self_index += b as usize;
        }
        Ok(())
    }

    /// Treat this as an indexed collection: find the location of item n.
    pub fn get(&self, target: usize) -> Option<ItemLocation> {
        let mut n = 0;
        for item in &self.0 {
            match *item {
                ItemIndex::Item(l) if n == target => { return Some(l); },
                ItemIndex::Item(_) => { n += 1; },
                ItemIndex::BlockDelim => {  },
            };
        }

        None
    }

    /// Get the number of items indexed in the header.
    pub fn len(&self) -> usize {
        self.0.len()
    }

    /// Remove the n'th item from the in-memory table. No files are updated.
    pub fn unlink(&mut self, n: usize) {
        self.0.remove(n);
    }

    /// Link a new element to the header given its size and a target index, then
    /// flush the header to the disk. If everything goes well (no fs errors),
    /// the byte index to write the element at is returned.
    pub fn link(
        &mut self, file: &mut File, idx: usize, len: u64,
    ) -> Result<u64> {
        // calculate the byte to insert `target` at in the file. This is always
        // just beyond the last element. Benchmarking TODO: is it better to
        // insert this after i - 1? Writes would be slower, but it would be more
        // cache-friendly.
        let target_byte = self.0.last().map(|x| match x {
            ItemIndex::Item(l) =>
                l.offset + l.length + size_of::<ItemIndex>() as u64,
            _ => panic!("Unreachable state. Block delim must always be followed with indices."),
        }).unwrap_or(0);

        self.0.insert(idx + 1, ItemIndex::Item(ItemLocation {
            offset: target_byte,
            length: len,
        }));

        self.write(file)?;

        Ok(target_byte)
    }

    /// Write a header block to the filesystem. A pre-existing block at
    /// `file[file_from]` is assumed, and the number of bytes that the block
    /// allows is written from `self.0[self_from]`.
    ///
    /// Returns:
    ///
    /// + if an IO error was encountered: `Err`
    /// + if the write was incomplete: `Ok(Some(next_block_location,  num_written)))`
    /// + if the write was complete: `Ok(None)`
    ///
    /// Assuming no IO errors, the next block's location in the file is
    /// returned, followed by the number of items written.
    fn write_block(
        &self, file: &mut File, self_from: usize, file_from: u64
    ) -> Result<Option<(u64, u64)>> {
        let mut u64_buffer = [0; 8];

        file.seek(SeekFrom::Start(file_from))?;
        file.read_exact(&mut u64_buffer)?;
        let len = u64::from_le_bytes(u64_buffer);

        file.read_exact(&mut u64_buffer)?;
        let next = u64::from_le_bytes(u64_buffer);

        // read `len` u64's
        let mut disk_block_buffer = vec![0; (len * 8).try_into().unwrap()];
        file.read_exact(&mut disk_block_buffer)?;

        let mut disk_block_ints = Vec::with_capacity(len.try_into().unwrap());
        for i in 0..(len as usize) {
            disk_block_ints.push(u64::from_le_bytes(
                disk_block_buffer[(i * 8)..((i + 1) * 8)].try_into().expect("8 bytes")
            ));
        }

        let buff = self.0.iter().skip(self_from);
        let disk = disk_block_ints.iter().copied();

        let new_len = self.0.len();
        if len != new_len.try_into().unwrap() {
            // TODO: is this worth the branch to maybe avoid the syscall?
            file.seek(SeekFrom::Start(file_from))?;
            file.write_all(&new_len.to_le_bytes())?;
        }

        let mut target: u64 = file_from as u64 + 16; // skip the length and the next
        for (i, (self_idx, disk_idx)) in buff.zip(disk).enumerate() {
            match self_idx {
                ItemIndex::BlockDelim => {
                    return Ok(Some((next, i.try_into().unwrap())));
                },
                ItemIndex::Item(ItemLocation {
                    length: _, offset,
                }) if *offset != disk_idx => {
                    file.seek(SeekFrom::Start(target))?;
                    file.write_all(&offset.to_le_bytes())?;
                },
                _ => {},
            }

            target += 8;
        }

        Ok(None)
    }

    /// Given a file and a `usize` index of the header, parse the block,
    /// returning read values and the address of the next block, if it exists.
    fn parse_header_block(
        f: &mut File, idx: u64,
    ) -> Result<
        (SmallVec<[ItemIndex; QUEUE_HEADER_INIT_SIZE]>, Option<NonZeroU64>)
    > {
        let mut v = SmallVec::new();
        let mut u64_buffer = [0; 8];

        f.seek(SeekFrom::Start(idx))?;

        f.read_exact(&mut u64_buffer)?;
        let len = u64::from_le_bytes(u64_buffer);

        f.read_exact(&mut u64_buffer)?;
        let next = NonZeroU64::new(u64::from_le_bytes(u64_buffer));

        for _ in 0..len {
            f.read_exact(&mut u64_buffer)?;
            let offset = u64::from_le_bytes(u64_buffer);
            f.read_exact(&mut u64_buffer)?;
            let length = u64::from_le_bytes(u64_buffer);

            v.push(ItemIndex::Item(ItemLocation { offset, length }));
        }

        Ok((v, next))
    }
}

