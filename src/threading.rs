// This Source Code Form is subject to the terms of the Mozilla Public License,
// v. 2.0. If a copy of the MPL was not distributed with this file, You can
// obtain one at https://mozilla.org/MPL/2.0/.

use std::marker;
use std::sync::atomic::AtomicU32;
use std::sync::atomic::Ordering;
use std::sync::mpsc;
use std::thread;

/// A TaskPool serially operates a set of operations in parallel to the primary
/// thread it's spawned on.
pub struct TaskPool<R: 'static + marker::Send + marker::Sync> {
    resl_queue: mpsc::Receiver<R>,
    work_queue: mpsc::Sender<Box<dyn Send + FnOnce() -> R>>,
    waiting_on: AtomicU32,
}

impl<R: 'static + marker::Send + marker::Sync> TaskPool<R> {
    /// Allocate an empty TaskPool.
    pub fn empty() -> Self {
        let (resl_queue_tx, resl_queue_rx) = mpsc::channel();
        let (work_queue_tx, work_queue_rx) = mpsc::channel();

        thread::spawn(move || {
            aux_thread(work_queue_rx, resl_queue_tx);
        });

        Self {
            resl_queue: resl_queue_rx,
            work_queue: work_queue_tx,
            waiting_on: AtomicU32::new(0),
        }
    }

    pub fn run<T: 'static + FnOnce() -> R + marker::Send>(&mut self, op: T) {
        self.work_queue.send(Box::new(op)).unwrap();
        self.waiting_on.fetch_add(1, Ordering::SeqCst);
    }

    /// Wait until the queue of work is empty, returning their results.
    pub fn join(&mut self) -> Vec<R> {
        let queued = self.waiting_on.load(Ordering::SeqCst);
        let mut out = Vec::new();

        for _ in 0..queued {
            out.push(self.resl_queue.recv().unwrap());
        }

        self.waiting_on.store(0, Ordering::SeqCst);

        out
    }
}

fn aux_thread<R: 'static + marker::Send + marker::Sync>(
    work: mpsc::Receiver<Box<dyn Send + FnOnce() -> R>>,
    resl: mpsc::Sender<R>,
) {
    loop {
        let task = work.recv().unwrap();
        resl.send(task()).unwrap();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn basic_pooling() {
        let mut tp = TaskPool::empty();
        tp.run(|| 2 + 2);
        tp.run(|| 1 + 1);
        let result = tp.join();
        thread::spawn(move || tp.run(|| 1 + 5));
        assert_eq!(result, vec![4, 2]);
    }
}
