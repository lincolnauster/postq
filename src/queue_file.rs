// This Source Code Form is subject to the terms of the Mozilla Public License,
// v. 2.0. If a copy of the MPL was not distributed with this file, You can
// obtain one at https://mozilla.org/MPL/2.0/.

use std::fs::{File, OpenOptions};
use std::io::{Read, Write, Seek, SeekFrom};
use std::path::PathBuf;

use super::Result;
use header::*;

mod header;

/// Encapsulate the file format we use. A header block is stored at the top of
/// the document storing, in order:
/// 1. A 32 bit length value *n* storing the number of following position
/// elements in the header
/// 2. A u64 indicating the address of the next header block, or 0 if none.
/// 3. *n* u32's indicating the positions of each queued item
///
/// Then, we queue new items at the bottom of the file and insert into the
/// header. We can dequeue items with similar maintenance to the header.
pub struct QueueFile {
    file: File,
    header: Header,
}

impl QueueFile {
    pub fn new(path: PathBuf) -> Result<Self> {
        let mut file = OpenOptions::new()
            .read(true)
            .write(true)
            .open(path)?;

        let header = Header::parse_header(&mut file)?;

        Ok(Self { file, header })
    }

    /// Pop the top of the queue, properly maintaining the header. This is an
    /// O(n) operation. This keeps empty space in the file where the head was.
    /// To remove it, use [TODO] the clean method.
    pub fn dequeue(&mut self) -> Result<Option<Vec<u8>>> {
        let val = match self.get(0)? {
            Some(v) => v,
            None => { return Ok(None); },
        };

        self.header.unlink(0);
        self.header.write(&mut self.file)?;

        Ok(Some(val))
    }

    /// Push to the back of the queue, properly maintaining the header. This is
    /// an O(n) operation due to the header traversal.
    pub fn enqueue(&mut self, target: &[u8]) -> Result<()> {
        let idx = self.header.len().saturating_sub(1);
        let tar = self.header.link(&mut self.file, idx, target.len() as u64)?;
        self.file.seek(SeekFrom::Start(tar))?;
        self.file.write_all(target)?;
        Ok(())
    }

    /// Treat the store like an array, and grab an indexed value, which is just
    /// treated like some bytes.
    pub fn get(&mut self, i: usize) -> Result<Option<Vec<u8>>> {
        let idx = match self.header.get(i) {
            Some(n) => n,
            None => return Ok(None),
        };

        self.file.seek(SeekFrom::Start(idx.offset))?;

        let mut data = vec![0; idx.length.try_into().unwrap()];
        self.file.read_exact(&mut data)?;
        Ok(Some(data))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs::remove_file;
    use std::io::Write;

    #[test]
    fn basic_get() {
        const PATH: &str = "/tmp/postq-testing-queue_file-basic_init.bin";
        let data = [
            0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 2 values
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // no next
            0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // first value index
            0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 4 bytes
            0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // second value index
            0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 4 bytes
            0x01, 0x00, 0x00, 0x00, // store 1 in a 32-bit int
            0x02, 0x00, 0x00, 0x00, // store 2 in a 32-bit int
        ];

        File::create(PATH)
            .unwrap()
            .write_all(&data)
            .ok();

        let mut q = QueueFile::new(PathBuf::from(PATH)).unwrap();
        assert_eq!(q.get(0).unwrap().unwrap(), [0x01, 0x00, 0x00, 0x00]);
        assert_eq!(q.get(1).unwrap().unwrap(), [0x02, 0x00, 0x00, 0x00]);
        assert_eq!(q.get(2).unwrap(), None);

        remove_file(PATH).unwrap();
    }

    #[test]
    fn basic_get_with_linked_header() {
        const PATH: &str = "/tmp/postq-testing-queue_file-basic_get_linked_header.bin";
        let data = [
            0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 2 values

            0x38, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // next

            0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // first value index
            0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // first value size

            0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // second value index
            0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // second value size

            0x01, 0x00, 0x00, 0x00, // store 1 in a 32-bit int
            0x02, 0x00, 0x00, 0x00, // store 2 in a 32-bit int

            0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 2 values

            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // no next
            0x68, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // first value index
            0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // first value size

            0x6c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // second value index
            0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // second value size

            0x03, 0x00, 0x00, 0x00,
            0x04, 0x00, 0x00, 0x00,
        ];

        File::create(PATH)
            .unwrap()
            .write_all(&data)
            .ok();

        let mut q = QueueFile::new(PathBuf::from(PATH)).unwrap();
        assert_eq!(q.get(0).unwrap().unwrap(), [0x01, 0x00, 0x00, 0x00]);
        assert_eq!(q.get(1).unwrap().unwrap(), [0x02, 0x00, 0x00, 0x00]);
        assert_eq!(q.get(2).unwrap().unwrap(), [0x03, 0x00, 0x00, 0x00]);
        assert_eq!(q.get(3).unwrap().unwrap(), [0x04, 0x00, 0x00, 0x00]);

        remove_file(PATH).unwrap();

    }

    #[test]
    fn empty_get() {
        const PATH: &str = "/tmp/postq-testing-queue_file-empty.bin";
        let data = [
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        ];

        File::create(PATH)
            .unwrap()
            .write_all(&data)
            .ok();

        let mut q = QueueFile::new(PathBuf::from(PATH)).unwrap();
        assert_eq!(q.get(0).unwrap(), None);

        remove_file(PATH).unwrap();
    }

    #[test]
    fn basic_enqueue() {
        const PATH: &str = "/tmp/postq-testing-queue_file-basic_enqueue.bin";
        let data = [
            0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 2 values
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // no next
            0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // first value index
            0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 4 bytes
            0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // second value index
            0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 4 bytes
            0x01, 0x00, 0x00, 0x00, // store 1 in a 32-bit int
            0x02, 0x00, 0x00, 0x00, // store 2 in a 32-bit int
        ];

        File::create(PATH)
            .unwrap()
            .write_all(&data)
            .ok();

        let mut q = QueueFile::new(PathBuf::from(PATH)).unwrap();
        q.enqueue(&[0x03, 0x00, 0x00, 0x00]).expect("filesystem to work");
        assert_eq!(q.dequeue().unwrap().unwrap(), [0x01, 0x00, 0x00, 0x00]);
        assert_eq!(q.dequeue().unwrap().unwrap(), [0x02, 0x00, 0x00, 0x00]);
        assert_eq!(q.dequeue().unwrap().unwrap(), [0x03, 0x00, 0x00, 0x00]);
        assert_eq!(q.dequeue().unwrap(), None);

        remove_file(PATH).unwrap();
    }
    #[test]
    fn basic_dequeue() {
        const PATH: &str = "/tmp/postq-testing-queue_file-basic_dequeue.bin";
        let data = [
            0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 2 values
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // no next
            0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // first value index
            0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 4 bytes
            0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // second value index
            0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 4 bytes
            0x01, 0x00, 0x00, 0x00, // store 1 in a 32-bit int
            0x02, 0x00, 0x00, 0x00, // store 2 in a 32-bit int
        ];

        File::create(PATH)
            .unwrap()
            .write_all(&data)
            .ok();

        let mut q = QueueFile::new(PathBuf::from(PATH)).unwrap();
        assert_eq!(q.dequeue().unwrap().unwrap(), [0x01, 0x00, 0x00, 0x00]);
        assert_eq!(q.dequeue().unwrap().unwrap(), [0x02, 0x00, 0x00, 0x00]);
        assert_eq!(q.dequeue().unwrap(), None);

        remove_file(PATH).unwrap();
    }

    // Verify that the produced files are able to be reopened.
    #[test]
    fn reinterpret() {
        const PATH: &str = "/tmp/postq-testing-queue_file-reinterpret.bin";
        let data = [
            0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 2 values
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // no next
            0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // first value index
            0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 4 bytes
            0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // second value index
            0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 4 bytes
            0x01, 0x00, 0x00, 0x00, // store 1 in a 32-bit int
            0x02, 0x00, 0x00, 0x00, // store 2 in a 32-bit int
        ];

        File::create(PATH)
            .unwrap()
            .write_all(&data)
            .ok();

        let mut q = QueueFile::new(PathBuf::from(PATH)).unwrap();
        assert_eq!(q.dequeue().unwrap().unwrap(), [0x01, 0x00, 0x00, 0x00]);

        let mut q = QueueFile::new(PathBuf::from(PATH)).unwrap();
        assert_eq!(q.dequeue().unwrap().unwrap(), [0x02, 0x00, 0x00, 0x00]);
        assert_eq!(q.dequeue().unwrap(), None);
    }
}
