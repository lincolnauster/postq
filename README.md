# postq
<img src="https://gitlab.com/lincolnauster/postq/badges/dev/pipeline.svg"/>

A tiny persistent FIFO queue

## Overview
Postq provides a small, simple, and generally easy-to-wrangle queue persisted
via the filesystem. It aims to be fast --- both in theory and practice --- by
leveraging few heap allocations, an in-memory cache, and asynchronous file IO
while retaining complete memory- and thread-safety.

## Usage
```rust
use postq::Queue;

fn main() {
	let q = Queue::new("/path/to/queue/file");
	q.enqueue(4);
	q.enqueue(1);
	assert_eq!(q.dequeue(), 4);
	assert_eq!(q.dequeue(), 1);
} // the queue is synced with the filesystem on exit
```

## Performance (much of this TODO)
`postq::Queue` maintains two resources: a stack-allocated buffered cache
supplements a filesystem-backed storage point. When data is manipulated, the
change is made to the in-memory cache in real time while the file is modified in
a concurrent (and usually parallel) thread. The `sync` method can be called to
make sure the file matches the buffer (more specifically, that all operations
applied to the queue have been mirrored on-disk), and is called automatically
whenever the queue is dropped.

## Compatibility
All (?) >= 32 bit platforms that the Rust standard library can build for are
supported.

If you're hacking on this library, development requires an OS with a /tmp
directory for running tests, which notably excludes Windows.

## 1.0.0 Checklist
All of the below need to be completed before I'll call this stable.

- [ ] Comprehensive benchmark suite for optimization
- [ ] Performance characteristics are stable and well-documented
- [ ] Working client code in Amp (for presistent undo-redo behavior)
- [ ] Good API for variably-sized elements

## Post 1.0.0 Roadmap
+ Using this in Rust is nice, but it would also be nice to be able to use this
  in whatever language I so please, which means I need to write nice C API
  bindings: lots of wildly unsafe type logic, coming soon to a C binding near
  you!

## Internals And Metrics
All necessary information (docs, code coverage, etc) is either in the repo
itself or rendered here[^1] on each commit.

[^1]: https://lincolnauster.gitlab.io/postq/
